import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Double tab[] = new Double[5];
        Random random = new Random();
        for(int i=0; i<5; i++) {
            tab[i] = random.nextDouble()+random.nextInt()%10;
        }
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Entrez le nom de votre Algorithme : ");
        String algo = sc.nextLine();
        System.out.println("Vous avez entré : " + algo);
        sc.close();

        Class classe1   = Class.forName(algo);
        Class classe2   = Class.forName("Afficher");    
        if (Algorithme.class.isAssignableFrom(classe1)) {
            System.out.println("Application de l'algorithme " + algo + " sur votre tableau");
            Object o1           = classe1.newInstance();
            Object o2           = classe2.newInstance();
            Algorithme trier    = (Algorithme) o1;
            Algorithme afficher = (Algorithme) o2;
            System.out.println("Affichage du tableau avant le tri");
            afficher.appliquer(tab);
            trier.appliquer(tab);
            System.out.println("Affichage du tableau après le tri");
            afficher.appliquer(tab);
        } else {
            System.out.println(algo + "n'implémente pas l'interface Algorithme");
        }
    }
}