public interface Algorithme<Obj extends Comparable> {

    public void appliquer(Obj d[]);
}