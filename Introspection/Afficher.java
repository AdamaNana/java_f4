public class Afficher<Obj extends Comparable> implements Algorithme<Obj> {

    public Afficher() {
    }

    public void appliquer(Obj tab[]) {
        for(int i=0; i<tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }
}