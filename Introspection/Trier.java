import java.util.Arrays;

public class Trier<Obj extends Comparable> implements Algorithme<Obj> {

    public Trier() {
    }

    public void appliquer(Obj tab[]) {
        Arrays.sort(tab);
    }
}