public class Tableau<Obj extends Comparable> {

    public Tableau(int c) {
        taille   = 0;
        capacite = c;
        tab      = (Obj[]) new Comparable[capacite];
    }

    public int getTaille() {
        return taille;
    }

    public int getCapacite() {
        return capacite;
    }

    public void agrandir() {
        try {
            Obj[] newtab = (Obj[]) new Comparable[capacite*2];;
            System.arraycopy(tab, 0, newtab, 0, taille);
            tab = newtab;
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Impossible d'agrandir le tableau");
        }
    }

    public void addElement(Obj o) {
        try {
            if (taille == capacite) {
                agrandir();
            }

            int i = taille-1;
            while (i >= 0 && tab[i].compareTo(o) > 0) {
                tab[i+1] = tab[i];
                i--;
            }
            tab[i+1] = o;
            taille++;
        } catch (OutOfMemoryError e) {
            System.out.println("Erreur : Mémoire insuffisante pour ajouter un élément");
        }
    }

    public Obj getElementAt(int index) throws MyArrayOutOfBoundsException {
        if (index < 0 || index >= taille) {
            throw new MyArrayOutOfBoundsException("Index " + index + " is out of bounds");
        }
        return tab[index];
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i=0; i<taille; i++) {
            sb.append(tab[i]);
            sb.append(" ");
        }
        return sb.toString();
    }

    private int taille;
    private int capacite;
    private Obj[] tab;
}