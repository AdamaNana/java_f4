public class Main {
    public static void main(String[] args) throws Exception {
        Tableau<Integer> monTableau = new Tableau<>(10);
        monTableau.addElement(100);
        monTableau.addElement(90);
        monTableau.addElement(80);
        monTableau.addElement(70);
        monTableau.addElement(60);
        monTableau.addElement(50);
        monTableau.addElement(40);
        monTableau.addElement(30);
        monTableau.addElement(20);
        monTableau.addElement(10);
        monTableau.addElement(0);

        int somme = 0;
        for (int i = 0; i < monTableau.getTaille(); i++) {
            Object object = monTableau.getElementAt(i);
            if (object instanceof Integer) {
                somme += (Integer) object;
            } else {
                System.out.println("Chelou");
            }
        }
        System.out.println(monTableau.toString());
        System.out.println("La somme des éléments du tableau est : " + somme);

        Tableau<Point> points = new Tableau<>(10);
        Point p1 = new Point(2, 2);
        Point p2 = new Point(1, 2);
        Point p3 = new Point(2, 4);
        Point p4 = new Point(0, 1);
        System.out.println(p1.toString());
        points.addElement(p1);
        points.addElement(p2);
        points.addElement(p3);
        points.addElement(p4);
        System.out.println(points.toString());
    }
}