public class Point implements Comparable<Point> {

    public Point(double xx, double yy) {
        x = xx;
        y = yy;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(x="+ x + " et y=" + y + ")";
    }

    @Override
    public int compareTo(Point p) {
        double xx   = Math.sqrt(x*x + y*y);
        double yy   = Math.sqrt((p.getX())*(p.getX()) + (p.getY())*(p.getY()));
        int bool = 0;
        if (xx < yy) {
            bool = -1;
        } else if (xx > yy) {
            bool = 1;
        } else {
            bool = 0;
        }
        return bool; 
    }

    private double x;
    private double y;
}