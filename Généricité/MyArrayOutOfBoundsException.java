class MyArrayOutOfBoundsException extends Exception {
    public MyArrayOutOfBoundsException(String s) {
        super(s);
    }
}