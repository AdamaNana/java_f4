import java.util.Vector; 

public class Bibliotheque {
    public static final int CAPACITE = 4;

    public Bibliotheque() {
    }

    public void ajouter(Livre l) {
        if (nbLivres == CAPACITE) {
            throw new ArrayIndexOutOfBoundsException("La Bibliothèque est pleine");
        } else {
            Biblio.add(l);
            nbLivres++;
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<nbLivres; i++) {
            sb.append((Biblio.get(i)).toString());
            sb.append(" ");
        }
        return sb.toString();
    }

    public int getNbLivres() {
        return nbLivres;
    }

    protected static int nbLivres;
    private Vector<Livre> Biblio = new Vector<Livre>(CAPACITE);
}