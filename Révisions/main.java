public class main {
    public static void main(String[] argv) {
        java.lang.System.out.println("Bonjour les zinzins");
        Livre titeuf           = new Livre("titeuf", 25);
        Livre toto             = new Livre("toto", 50);
        Livre ken              = new Livre("zola", 75);
        Livre  l               = new Livre("manathan", 100);

        LivreNumerique  ln     = new LivreNumerique("Dolet", 15, Format.PDF);

        Bibliotheque   b       = new Bibliotheque();
        Bibliothecaire adama   = new Bibliothecaire("Nana", "Adama");

        adama.ajouter(titeuf, b);
        adama.ajouter(toto, b);
        adama.ajouter(ken, b);
        adama.ajouter(l, b);

        java.lang.System.out.println(b.toString());
        //java.lang.System.out.println(l.getisbn());
    }
}