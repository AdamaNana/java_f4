public class Livre {
    protected String titre;
    protected double prix;
    protected static int isbn;

    public Livre (String str, double p) {
        titre = str;
        prix   = p;
        isbn++;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String str) {
        titre = str;
    }

    public double getPrix() {
        return this.prix;
    }

    public void setPrix(double p) {
        prix = p;
    }

    public int getisbn() {
        return isbn;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(titre);
        sb.append(" ");
        sb.append(prix);
        return sb.toString();
    }

}