public class Bibliothecaire extends Personne implements Gestion {
    public Bibliothecaire(String n, String p) {
        super(n, p);
    }
    
    public void ajouter(Livre l, Bibliotheque Biblio) {
        Biblio.ajouter(l);
    }
}