public class Personne {
    public Personne(String n, String p) {
        Nom    = n;
        Prenom = p;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(Nom);
        sb.append(" ");
        sb.append(Prenom);
        return sb.toString();
    }

    private String Nom;
    private String Prenom;
}