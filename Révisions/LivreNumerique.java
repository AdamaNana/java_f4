enum Format { 
    PDF, EPUB, HTML
}

public class LivreNumerique extends Livre {
    public LivreNumerique(String str, double p, Format f) {
        super(str, p);
        format = f;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(titre);
        sb.append(" ");
        sb.append(format);
        sb.append(" ");
        sb.append(prix);
        return sb.toString();
    }

    private Format format;
}