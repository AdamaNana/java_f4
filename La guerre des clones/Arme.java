public abstract class Arme {
    private int NumSerie;
    private static int cmp;

    public Arme() {
        NumSerie = cmp+1000;
        cmp++;
    }

    public int getSerie() {
        return NumSerie;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(" ");
        sb.append(NumSerie);
        return sb.toString();
    }
}