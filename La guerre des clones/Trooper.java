import java.util.Random;

enum Grade {
    TROOPER {
        public String getMarque() { return "WHITE"; } 
    },

    SERGENT {
        public String getMarque() { return "GREEN"; } 
    },

    LIEUTENANT {
        public String getMarque() { return "BLUE"; } 
    },

    CAPITAINE {
        public String getMarque() { return "RED"; } 
    },

    COMMANDEUR {
        public String getMarque() { return "YELLOW"; } 
    };

    abstract public String getMarque();
}

public class Trooper implements Cloneable {
    public int Id;
    public String Nom = "Clone de Jango Fett #" + Id;
    public int Sante;
    public Arme Arme;
    public Ceinture ceinture;
    private static int cmp;
    public Grade Grade;

    public Trooper (String nom, Arme arme, Ceinture c, Grade g) {
        Random random = new Random();
        Id            = cmp;
        Nom           = nom;
        Sante         = random.nextInt(51) + 50;
        Arme          = arme;
        ceinture      = c;
        Grade         = g;
        cmp++;
    }

    public Object clone() {
        Trooper object = null;
        try {
            object = (Trooper) super.clone();
        } catch(CloneNotSupportedException cnse) {
            cnse.printStackTrace(System.err);
        }
        // s'occuper des attributs "compliqués" 
        // si object est non null
        // MAIS PAS TOUT DE SUITE
        // uniquement quand on vous le demande

        return object;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Identifiant :");
        sb.append(Id);
        sb.append("; ");
        sb.append("Nom :");
        sb.append(Nom);
        sb.append("; ");
        sb.append("Niveau de santé :");
        sb.append(Sante);
        sb.append("; ");
        sb.append("Arme :");
        sb.append(Arme.toString());
        sb.append("; ");
        sb.append("Grade :");
        sb.append(Grade.getMarque());
        return sb.toString();
    }
}