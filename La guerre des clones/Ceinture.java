import java.util.Vector; 

public class Ceinture {

    private Vector<String> Kit = new Vector<String>(20);

    public Ceinture() {
    }

    public void ajouter(String s) {
        if(Kit.size()==20) {
            throw new ArrayIndexOutOfBoundsException("La Ceinture est pleine");
        } else {
            Kit.add(s);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i=0; i<Kit.size(); i++) {
            sb.append(" ");
            sb.append(Kit.get(i));
        }
        return sb.toString();
    }
}