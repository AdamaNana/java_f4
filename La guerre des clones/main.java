public class main {
    public static void main(String[] argv) {
        java.lang.System.out.println("Bonjour les zinzins");

        Ceinture c = new Ceinture();
        c.ajouter("kit médical");
        c.ajouter("Grappin");
        c.ajouter("Munitions");
        c.ajouter("Grenade");
        c.ajouter("Radio");

        Fusil f = new Fusil("Blaster DC 15");

        Trooper cody = new Trooper("Cody", f, c, Grade.COMMANDEUR);
        java.lang.System.out.println(cody.toString());
    }
}