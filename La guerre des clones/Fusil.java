public class Fusil extends Arme {

    private String Nom;

    public Fusil(String nom) {
        super();
        Nom = nom;
    }

    public String toString() {
        return Nom + " no Série" + super.toString();
    }
}